
#include "dwallpaper.h"

int command_execute(char* s_command, char* ipath)
{
	char finalCommand[PATH_MAX] = {0};
	size_t i;
	size_t offset = 0;

	for (i = 0; i < strlen(s_command); i++)
	{
		if (s_command[i] == 0)
			break;

		if (s_command[i] == '%')
		{
			char* temp = malloc(sizeof(char) * strlen(ipath) + 3);
			sprintf(temp, "'%s'", ipath);
			strcat(finalCommand, temp);
			offset += strlen(temp) - 1;
			free(temp);
		}
		else
			finalCommand[offset++] = s_command[i];
	}
	finalCommand[offset + 1] = 0;

	syslog(LOG_INFO, "Executing command: '%s'", finalCommand);

	return system(finalCommand);
}

