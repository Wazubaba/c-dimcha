#ifndef DIMCHA_DAEMON_WALLPAPER
#define DIMCHA_DAEMON_WALLPAPER

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <malloc.h>
#include <unistd.h>
#include <syslog.h>
#include <linux/limits.h>

int command_execute(char* s_command, char* ipath);

#endif /* ifndef DIMCHA_DAEMON_WALLPAPER */

