/*
	This file is part of dimcha.

	Desktop IMage CHAnger - dimcha
	Copyright © 2017 Wazubaba

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <string.h>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>

#include <argtable2.h>

#include "dcore.h"
#include "common/paths.h"

#define VERSION "1.0.0"
#define BUGMAIL "TBD"

struct arg_lit *aHelp, *aVerbose, *aVersion, *aForceConfig, *aSkipRunfileWrite;
struct arg_file *aRunfile, *aConfig, *aIla;
struct arg_end *end;

int main(int argc, char* argv[])
{

	short f_writeRunfileOnExit = 1;
	short f_verbose = 0;
	short f_forceConfigInit = 0;


	void *argtable[] =
	{
		aHelp = arg_lit0("h", "help", "Show help information and exit"),
		aForceConfig = arg_lit0("c", "force-config", "Force to initialize from config instead of last run file"),
		aSkipRunfileWrite = arg_lit0("r", "no-write-runfile", "Do not write a runfile on exit (all runtime settings will be lost)"),
		aVerbose = arg_lit0("v", "verbose", "More verbose logging"),
		aVersion = arg_lit0("V", "version", "Show version information and exit"),
		aRunfile = arg_file0("R", "runfile-path",  "<file>", "Override default runfile storage path"),
		aConfig = arg_file0("C", "config-path", "<file>", "Use specified config file instead of default"),
		aIla = arg_file0("I", "ila-path", "<ila file>", "Override config setting for ila file path"),
		end = arg_end(20),
	};

	if (arg_parse(argc, argv, argtable) > 0)
	{
		arg_print_errors(stderr, end, "dimcha_daemon");
		exit(5);
	}

	if (aHelp->count > 0)
	{
		printf("Usage: %s [OPTION...] ARG1 ARG2\n", argv[0]);
		puts("DIMCHA: Desktop IMage CHAnger daemon\n");
		arg_print_glossary(stdout, argtable, " %-25s %s\n");

		printf("\nMandatory or optional arguments to long options are");
		printf("also mandatory or optional for any corresponding short");
		printf("options.\n\nReport bugs to <%s>.\n", BUGMAIL);

		arg_freetable(argtable, sizeof(argtable)/sizeof(argtable[0]));

		return 0;
	}

	if (aVersion->count > 0)
	{
		printf("Dimcha Daemon version %s\n", VERSION);
		arg_freetable(argtable, sizeof(argtable)/sizeof(argtable[0]));
		return 0;
	}


	struct DaemonArgs dArgs = {NULL, NULL, NULL, 0, 0, 0};

	if (aForceConfig->count > 0) f_forceConfigInit = 1;
	if (aVerbose->count > 0) f_verbose = 1;
	if (aSkipRunfileWrite->count > 0) f_writeRunfileOnExit = 0;

	if (aRunfile->count > 0) dArgs.runfilePathOverride = (char*) *aRunfile->filename;
	if (aConfig->count > 0) dArgs.configPathOverride = (char*) *aConfig->filename;
	if (aIla->count > 0) dArgs.ilaPathOverride = (char*) *aIla->filename;

	arg_freetable(argtable, sizeof(argtable)/sizeof(argtable[0]));

	puts("here we go!");

	printf("Using runfile: %s\n", dArgs.runfilePathOverride);

	dArgs.f_verbose = f_verbose;
	dArgs.f_writeRunfileOnExit = f_writeRunfileOnExit;
	dArgs.f_forceConfigInit = f_forceConfigInit;

	path_init();

	daemon_start(&dArgs);


	return 0;
}

