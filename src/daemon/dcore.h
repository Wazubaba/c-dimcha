/*
	This file is part of dimcha.

	Desktop IMage CHAnger - dimcha
	Copyright © 2017 Wazubaba

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DIMCHA_DAEMON
#define DIMCHA_DAEMON

#include <string.h>
#include <syslog.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <pthread.h>
#include <signal.h>

#include <linux/limits.h>
#include <unistd.h>

#include "common/net.h"
#include "common/config.h"
#include "common/ila.h"
#include "common/paths.h"

#include "dwallpaper.h"

#define DEFAULT_RUNFILE "dimcha.runfile"
#define DEFAULT_CONFIG  "dimcha/dimcharc"
#define SOCKETNAME "/tmp/dimcha.socket"

/**
 * This module contains the core of dimcha.
*/

struct DaemonArgs
{
	char* configPathOverride;
	char* runfilePathOverride;
	char* ilaPathOverride;
	short f_verbose;
	short f_writeRunfileOnExit;
	short f_forceConfigInit;
};

int daemon_start(struct DaemonArgs* args);
//void daemon_start(char* configPathOverride, char* runfilePathOverride, short verboseFlag);

#endif /* ifndef DIMCHA_DAEMON */

