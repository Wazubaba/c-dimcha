/*
	This file is part of dimcha.

	Desktop IMage CHAnger - dimcha
	Copyright © 2017 Wazubaba

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "dcore.h"

pthread_mutex_t runningLock;

struct ThreadArgs
{
	struct Options* opts;
	struct ImageListArchive* ila;
	short* running;
	short* verboseFlag;
};

char* string_split_index(char* str, size_t index)
{
	size_t rhs_size = strlen(str) - index + 1;
	char* rhs = malloc(sizeof(char) * rhs_size);

	strncpy(rhs, str+index, rhs_size);
	rhs[rhs_size - 1] = '\0';

	return rhs;
}

/**
	This handler is a dummy just to let the cycle interrupt its sleep
	to process shutdowns or updates to the delay setting
*/
void daemon_cycle_sig_handler(int interrupt)
{
	if (interrupt) interrupt = 0; /* Just to stop compiler bitcherating :P */
}

/**
	This thread is what performs the main function of dimcha; i.e the
	random image selection and execution of the specified command.
*/
void* daemon_cycle(void* params)
{
	signal(SIGCONT, daemon_cycle_sig_handler);

	struct ThreadArgs* args = params;
	while (*args->running)
	{
		/* Perform the delay->imageswap->repeat loop */
		if (sleep(args->opts->delay) == 0)
		{
			char* imagePath = NULL;
			imagePath = ila_choose_image(args->ila);
			if (imagePath == NULL)
				syslog(LOG_WARNING, "Unable to select new image!");
			else
			{
				syslog(LOG_INFO, "Selected new image: '%s'", imagePath);

				if (command_execute(args->opts->command, imagePath) != 0)
					syslog(LOG_WARNING, "Unable to set wallpaper!");
			}
		}
		else
		{
//			syslog(LOG_INFO, "cycle-thread: delay is now %d, running is %hd", args->opts->delay, *args->running);
		}
	}
	syslog(LOG_INFO, "cycle-thread: exiting!");
	pthread_exit(NULL);
}

/**
	This function starts the cycle thread and a listening UNIX socket to
	accept commands. It also handles de-initialization across the board.
*/

int daemon_start(struct DaemonArgs* args)
{
	if (daemon(0, 1) == -1)
	{
		perror("Failed to daemonize!");
		return 2;
	}

	char configPath[PATH_MAX] = {0};
	char runfilePath[PATH_MAX] = {0};
	char ilaPath[PATH_MAX] = {0};
	pthread_mutex_init(&runningLock, NULL);

	/* Storage for the listener */
	int localfd = 0, remotefd = 0;
	char buffer[256];
	socklen_t remotelen = 0;

	struct sockaddr_un remote;
	memset((char*) &remote, 0, sizeof(struct sockaddr_un));

	/* This controls both the loop in the cycle thread and the listener */
	short running = 1;

	struct ImageListArchive ila;

	/* Handle config overrides */
	if (args->configPathOverride == NULL)
		path_resolve_config(configPath, DEFAULT_CONFIG);
	else
		path_resolve_local(configPath, args->configPathOverride);

	/* Handle runfile overrides */
	if (args->runfilePathOverride == NULL)
		path_resolve_cache(runfilePath, DEFAULT_RUNFILE);
	else
		path_resolve_local(runfilePath, args->runfilePathOverride);

	openlog("dimcha-daemon", LOG_PID, LOG_DAEMON);


	/* Attempt to load runfile */
	struct Options opts = {0, {0}, {0}, 0};
	int errcode;
	if (!args->f_forceConfigInit)
	{
		errcode = config_load(&opts, runfilePath);
		if (errcode > 0)
		{
			syslog(LOG_WARNING, "Failed to parse runfile %s, falling-back to config", runfilePath);
			goto tryConfig;
		}
	}
	else
	{
		if (args->f_verbose)
			syslog(LOG_INFO, "Skipping runfile and loading config '%s'", configPath);

		tryConfig:
		errcode = config_load(&opts, configPath);
		if (errcode > 0)
		{
			syslog(LOG_ERR, "Failed to load configuration from %s", configPath);
			syslog(LOG_NOTICE, "Shutdown");
			closelog();
			return 2;
		}
	}

	/* Handle ila overrides after config is loaded */
	if (args->ilaPathOverride == NULL)
	{
		if (opts.ilaPath[0] != '/') /* Only screw with it if it is not rooted */
			path_resolve_local(ilaPath, opts.ilaPath);
		else
			strcpy(ilaPath, opts.ilaPath);
	}
	else
		path_resolve_local(ilaPath, args->ilaPathOverride);
//		sprintf(ilaPath, "%s/%s", callingDirectory, args->ilaPathOverride);
	strcpy(opts.ilaPath, ilaPath);

	/* Initialize an ILA file */
	if (ila_import(&ila, ilaPath) > 0)
	{
		syslog(LOG_ERR, "Failed to load ilafile '%s'", ilaPath);
		syslog(LOG_NOTICE, "Shutdown");
		closelog();
		return 2;
	}

	if (args->f_verbose)
		syslog(LOG_INFO, "Using ilafile '%s'", opts.ilaPath);

	/* Initialize the cycles thread */
	struct ThreadArgs tArgs = {&opts, &ila, &running, &args->f_verbose};
	pthread_t cycleThread;
	pthread_create(&cycleThread, NULL, daemon_cycle, (void*) &tArgs);


	/* Initialize the listener socket */
	localfd = net_spawn_listener(SOCKETNAME);

	if (listen(localfd, 5) == -1)
	{
		syslog(LOG_ERR, "Failed to listen");
		syslog(LOG_NOTICE, "Shutdown");
		ila_destroy(&ila);
		closelog();
	}

	syslog(LOG_NOTICE, "Dimcha: Is now running! ^_^");

	while (running)
	{
		remotelen = sizeof(remote);

		if (!remotefd)
		{
			if ((remotefd = accept(localfd, (struct sockaddr*) &remote, &remotelen)) == -1)
			{
				syslog(LOG_ERR, "Failed to accept incoming connection");
				remotefd = 0;
			}
			if (args->f_verbose)
				syslog(LOG_INFO, "new connection");
		}
		else
		{
			//char* response = NULL;
			char response[256];
			int bytes = read(remotefd, buffer, 255);
			if (bytes < 0)
				syslog(LOG_ERR, "Failed to read from socket");

			// http://stackoverflow.com/questions/2693776/ddg#28462221
			buffer[strcspn(buffer, "\n")] = 0;
			buffer[strcspn(buffer, "\r\n")] = 0;

			buffer[bytes] = '\0';

			if (strncmp(buffer, "sd", 2) == 0)
			{
				if (args->f_verbose)
					syslog(LOG_INFO, "Shutdown command recieved!");

				strcpy(response, "00 Got shutdown command, goodbye \\o\n");
				pthread_mutex_lock(&runningLock);
				running = 0;
				pthread_mutex_unlock(&runningLock);
			} else
			if (strncmp(buffer, "cd", 2) == 0)
			{
				if (args->f_verbose)
					syslog(LOG_INFO, "Remote connection terminated");

				shutdown(remotefd, SHUT_RDWR);
				close(remotefd);
				remotefd = 0;
			} else
			if (strncmp(buffer, "delay", 5) == 0)
			{
				char* delaystr = string_split_index(buffer, 6);
				int newDelay = atoi(delaystr);
				free(delaystr);

				opts.delay = newDelay;
				pthread_kill(cycleThread, SIGCONT);

				if (opts.delay == newDelay)
				{
					if (args->f_verbose)
						syslog(LOG_INFO, "Delay settings changed to %d", opts.delay);

					strcpy(response, "Delay settings updated");
				}
			}

			else
				sprintf(response, "Got your message! '%s' \\o\n", buffer);


			bytes = write(remotefd, response, strlen(response));
			if (bytes < 0)
				syslog(LOG_ERR, "Error writing to socket");

			if (!running)
			{
				shutdown(remotefd, SHUT_RDWR);
				close(remotefd);
				break;
			}
		}

		/* NORMAL OPERATION STUFF GOES HERE */
	}

	if (args->f_writeRunfileOnExit)
	{
		if (config_save(&opts, runfilePath) > 0)
			syslog(LOG_ERR, "Failed to write runfile to %s", runfilePath);
		else
			if (args->f_verbose)
				syslog(LOG_INFO, "Wrote runfile to %s", runfilePath);
	}

	if (localfd != 0)
		if (!net_destroy_listener(localfd))
			syslog(LOG_ERR, "Failed to destroy listener socket :(");

	if (args->f_verbose)
		syslog(LOG_INFO, "Terminating cycle thread");

	int err = pthread_kill(cycleThread, SIGCONT);
	if (err > 0)
		syslog(LOG_ERR, "Could not kill cycle thread!");

	syslog(LOG_INFO, "Waiting for cycle thread to rejoin...");
	pthread_join(cycleThread, NULL);
	syslog(LOG_INFO, "Done, freeing resources and shutting down");

	ila_destroy(&ila);

	err = unlink(SOCKETNAME);
	if (err > 0)
		syslog(LOG_INFO, "Failed to delete listener socket");

	closelog();

	return 0;
}

