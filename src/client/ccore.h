/*
	This file is part of dimcha.

	Desktop IMage CHAnger - dimcha
	Copyright © 2017 Wazubaba

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DIMCHA_CLIENT
#define DIMCHA_CLIENT

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>


#include <sys/socket.h>
#include <sys/un.h>

#define SOCKETNAME "/tmp/dimcha.socket"
void client_start();

#endif /* ifndef DIMCHA_CLIENT */

