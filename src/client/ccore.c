/*
	This file is part of dimcha.

	Desktop IMage CHAnger - dimcha
	Copyright © 2017 Wazubaba

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ccore.h"

void client_start()
{
	int remotefd, bytes;
	struct sockaddr_un remote;
	char buffer[256];

	if ((remotefd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1)
	{
		perror("Failed to establish connection to daemon");
		exit(1);
	}

	printf("Connecting to %s...", SOCKETNAME);

	memset((char*) &remote, 0, sizeof(struct sockaddr_un));
	remote.sun_family = AF_UNIX;
	strcpy(remote.sun_path, SOCKETNAME);

	socklen_t len = strlen(remote.sun_path) + sizeof(remote.sun_family);

	if (connect(remotefd, (struct sockaddr*) &remote, len) == -1)
	{
		perror("FAILED");
		exit(1);
	}

	puts("SUCCESS");

	while(printf("> "), fgets(buffer, 256, stdin), !feof(stdin))
	{
		if (strncmp(buffer, "q", 1) == 0)
		{
			send(remotefd, "cd", 2, 0);
			puts("Terminating client only!");
			break;
		}

		if (send(remotefd, buffer, strlen(buffer), 0) == -1)
		{
			perror("Failed to send message to daemon");
			break;
		}

		if ((bytes = recv(remotefd, buffer, 255, 0)) > 0)
		{
			buffer[bytes] = '\0';
			if (strncmp(buffer, "00", 2) == 0)
			{
				puts("Recieved daemon shutdown notification");
				break;
			}
			else
				printf("echo> %s", buffer);
		}
		else
		{
			if (bytes < 0)
				perror("Failed to recv data from daemon");
			else
				puts("Daemon closed connection");
			break;
		}
	}

	shutdown(remotefd, SHUT_RDWR);
	close(remotefd);
}

