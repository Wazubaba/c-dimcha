/*
	This file is part of dimcha.

	Desktop IMage CHAnger - dimcha
	Copyright © 2017 Wazubaba

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "iview.h"

int ila_view(int argc, char* argv[])
{
	if (argc < 2)
	{
		fprintf(stderr, "%s: Missing operand\nUsage: %s [PATH|NAME]\n", argv[0], argv[0]);
		fprintf(stderr, "Path: path to a valid ila file\n");
		fprintf(stderr, "Name: name of an ila in the default directory\n");

		return 1;
	}

	char itarg[PATH_MAX];
	struct ImageListArchive ila;

	path_init();

	if (argv[1][0] == '/')
		strcpy(itarg, argv[1]);
	else
	{
		if (strchr(argv[1], '/') == NULL)
		{
			char finalSubPath[PATH_MAX];
			sprintf(finalSubPath, "dimcha/%s.ila", argv[1]);
			path_resolve_cache(itarg, finalSubPath);
		}
		else
			path_resolve_local(itarg, argv[1]);
	}

	if (ila_import(&ila, itarg) != 0)
	{
		fprintf(stderr, "Invalid ila path specified\n");
		return 1;
	}

	size_t i;
	printf("Contents of %s:\n", itarg);
	for (i = 0; i < ila.size; i++)
		printf("\t%s\n", ila.paths[i]);

	return 0;
}

