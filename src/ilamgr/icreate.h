/*
	This file is part of dimcha.

	Desktop IMage CHAnger - dimcha
	Copyright © 2017 Wazubaba

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DIMCHA_ILA_CREATE
#define DIMCHA_ILA_CREATE

#include <string.h>
#include <sys/stat.h>
#include <stdio.h>

#include <argtable2.h>

#include <linux/limits.h>

#include "common/ila.h"
#include "common/paths.h"

#include "iconfig.h"

int ila_create(int argc, char* argv[]);

#endif /* ifndef DIMCHA_ILA_CREATE */

