/*
	This file is part of dimcha.

	Desktop IMage CHAnger - dimcha
	Copyright © 2017 Wazubaba

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <linux/limits.h>

#include "icreate.h"
#include "iview.h"
#include "iconfig.h"

int main(int argc, char* argv[])
{
//	char arg0[PATH_MAX] = {0};
	char command[PATH_MAX] = {0};

//	strcpy(arg0, argv[0]);
	if (strlen(argv[0]) > 2)
	{
		/* Ensure path is stripped */
		char* delim = strrchr(argv[0], '/');
		strcpy(command, delim + 1);
	}

	/* Support arg0 control */
	if (strncmp(command, "ila-create", 10) == 0)
		return ila_create(argc, argv);
	else if (strncmp(command, "ila-view", 8) == 0)
		return ila_view(argc, argv);

	/*=====================*/
	if (argc < 2)
	{
		fprintf(stderr, "%s: missing command\n", argv[0]);
		return 1;
	}

	if (strncmp(argv[1], "--help", 6) == 0 || strncmp(argv[1], "-h", 2) == 0)
	{
		puts("Commands:\n\tcreate\n\tview");
		return 0;
	}


	/*=====================*/
	int sargc = argc - 1;
	char** sargv = malloc(sizeof(char*) * sargc);

	int i;
	for (i = 1; i < argc; i++)
	{
		sargv[i-1] = malloc(sizeof(char) * strlen(argv[i]));
		strcpy(sargv[i-1], argv[i]);
	}

	/* support stand-alone mode */


	int exitCode = 1;

	if (strncmp(argv[1], "create", 6) == 0)
		exitCode = ila_create(sargc, sargv);
	else if (strncmp(argv[1], "view", 4) == 0)
		exitCode = ila_view(sargc, sargv);
	else
		fprintf(stderr, "Invalid command, use the symlinks!\n");

	for (i = sargc; i > 0; i--)
		free(sargv[i]);

	free(sargv);
	
//	return create(argc, argv);
	return exitCode;
}


