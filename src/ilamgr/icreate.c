/*
	This file is part of dimcha.

	Desktop IMage CHAnger - dimcha
	Copyright © 2017 Wazubaba

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "icreate.h"

struct arg_lit *aHelp, *aVerbose, *aVersion;
struct arg_str *aName;
struct arg_file *aOutputPath, *aTargetPath;
struct arg_end *end;

int ila_create(int argc, char* argv[])
{
	short f_verbose = 0;

	void *argtable[] =
	{
		aHelp = arg_lit0("h", "help", "Show help information and exit"),
		aVerbose = arg_lit0("v", "verbose", "More verbose logging"),
		aVersion = arg_lit0("V", "version", "Show version information and exit"),
		aOutputPath = arg_file0("o", "output", "<PATH>", "Output generated ILA to specified PATH instead of default PATH"),
		aTargetPath = arg_file0(NULL, NULL, "<PATH>", "Path to index ila from"),
		aName = arg_str0(NULL, NULL, "<NAME>", "Name of generated ila file (overriden by -o)"),

		end = arg_end(20),
	};



	if (arg_parse(argc, argv, argtable) > 0)
	{
		arg_print_errors(stderr, end, "dimcha_daemon");
		return 1;
	}

	if (aHelp->count > 0)
	{
		printf("Usage: %s [OPTION...] PATH/TO/IMAGES NAME\n", argv[0]);
		puts("ILAMGR: Image List Archive Manager for Dimcha\n");
		arg_print_glossary(stdout, argtable, " %-25s %s\n");

		printf("\nMandatory or optional arguments to long options are");
		printf("also mandatory or optional for any corresponding short");
		printf("options.\n\nReport bugs to <%s>.\n", BUGMAIL);

		arg_freetable(argtable, sizeof(argtable)/sizeof(argtable[0]));

		return 0;
	}

	if (aVersion->count > 0)
	{
		printf("ILA Manager version %s\n", VERSION);
		arg_freetable(argtable, sizeof(argtable)/sizeof(argtable[0]));
		return 0;
	}

	if (aVerbose->count > 0)
		f_verbose = 1;

	path_init();
	if (argc < 3)
	{
		fprintf(stderr, "%s: missing operand(s)\n", argv[0]);
		fprintf(stderr, "Try '%s --help' for more information.\n", argv[0]);
		return 1;
	}

	char ilaPath[PATH_MAX] = {0};
	char targetPath[PATH_MAX] = {0};

	if (aOutputPath->count > 0)
	{
		char* temp = (char*) *aOutputPath->filename;
		if (strlen(temp) == 0)
		{
			fprintf(stderr, "Invalid path specified: '%s'", temp);
			return 2;
		}

		if (temp[0] == '/')
			strcpy(ilaPath, temp);
		else
		{
			if (strncmp((char*) *aOutputPath->extension, "ila", 3) != 0)
				strcat(temp, ".ila");
			path_resolve_local(ilaPath, temp);
		}
	}
	else
	{
		if (aName->count == 0)
		{
			fprintf(stderr, "%s: missing operand\nTry '%s --help' for more information.\n", argv[0], argv[0]);
			return 1;
		}
		char finalSubPath[PATH_MAX] = {0};
		sprintf(finalSubPath, "dimcha/%s.ila", (char*) *aName->sval);
		path_resolve_cache(ilaPath, finalSubPath);
	}

	if (aTargetPath->count == 0)
	{
		fprintf(stderr, "%s: missing operand\nTry '%s --help' for more information.\n", argv[0], argv[0]);
		return 1;
	}

	char* temp = (char*) *aTargetPath->filename;
	if (strlen(temp) == 0)
	{
		fprintf(stderr, "%s: invalid target path '%s'\nTry '%s --help' for more information.\n", argv[0], temp, argv[0]);
		return 1;
	}
	if (temp[0] == '/')
		strcpy(targetPath, temp);
	else
		path_resolve_local(targetPath, temp);

	if (f_verbose) printf("Target: %s\nIla: %s\n", targetPath, ilaPath);

	struct ImageListArchive ila;
	if (ila_generate_from_path(&ila, targetPath))
		return 2;

	ila_export(&ila, ilaPath);

	ila_destroy(&ila);
	
	arg_freetable(argtable, sizeof(argtable)/sizeof(argtable[0]));
	return 0;
}

