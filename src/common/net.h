/*
	This file is part of dimcha.

	Desktop IMage CHAnger - dimcha
	Copyright © 2017 Wazubaba

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DIMCHA_NET
#define DIMCHA_NET

#include <bsd/string.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <sys/un.h>
#include <unistd.h>

#include <stdlib.h>

int error;

int net_spawn_listener();
int net_destroy_listener(int sockfd);
void net_start_listener(int sockfd);

#endif /* ifndef DIMCHA_NET */

