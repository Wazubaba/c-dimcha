/*
	This file is part of dimcha.

	Desktop IMage CHAnger - dimcha
	Copyright © 2017 Wazubaba

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "net.h"

int net_spawn_listener(char* path)
{
	int sockfd = 0; 
	if ((sockfd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1)
	{
		perror("Net: Failed to create listener socket");
		exit(1);
	}

	int sockFlags = fcntl(sockfd, F_GETFL, 0);

	if (sockFlags == -1)
	{
		perror("Net: Failed to get listener socket options");
		close(sockfd);
		exit(1);
	}

	if (fcntl(sockfd, F_SETFL, sockFlags & ~O_NONBLOCK) == -1)
	{
		perror("Net: Failed to set listener socket to non-blocking");
		close(sockfd);
		exit(1);
	}

	struct sockaddr_un localaddr;
	memset((char*) &localaddr, 0, sizeof(localaddr));

	localaddr.sun_family = AF_UNIX;
	strcpy(localaddr.sun_path, path);
	unlink(localaddr.sun_path);

	socklen_t len = strlen(localaddr.sun_path) + sizeof(localaddr.sun_family);

	if (bind(sockfd, (struct sockaddr*) &localaddr, len) == -1)
	{
		perror("Net: Failed to bind listener socket");
		close(sockfd);
		exit(1);
	}
	
	return sockfd;
}

int net_destroy_listener(int sockfd)
{
	if (shutdown(sockfd, SHUT_RDWR))
	{
		perror("Net: Failed to shutdown listener socket");
		return 0;
	}

	if (close(sockfd))
	{
		perror("Net: Failed to free listener socket");
		return 0;
	}

	return 1;
}

void net_start_listener(int sockfd)
{

	socklen_t clilen;
	int clientfd;
	char buffer[256];
	struct sockaddr_un remoteaddr;
	memset((char*) &remoteaddr, 0, sizeof(remoteaddr));


	listen(sockfd, 5);
	clilen = sizeof(remoteaddr);

	clientfd = accept(sockfd, (struct sockaddr*) &remoteaddr,
			&clilen);

	if (clientfd < 0)
		perror("Failed to accept incoming clinet!");

	memset((char*) &buffer, 0, sizeof(buffer));

	int bytes = 0;
	bytes = read(clientfd, buffer, 255);
	if (bytes < 0) perror("ERROR reading from socket");
	printf("Here is the message: %s\n", buffer);
	bytes = write(clientfd, "I got your message", 18);
	if (bytes < 0) perror("ERROR writing to socket");

	if (shutdown(clientfd, SHUT_RDWR))
		perror("Failed to shutdown client socket");

	if (close(clientfd))
		perror("Failed to free client socket");
}
