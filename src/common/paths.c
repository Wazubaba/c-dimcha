/*
	This file is part of dimcha.

	Desktop IMage CHAnger - dimcha
	Copyright © 2017 Wazubaba

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "paths.h"

void path_init()
{
	paths_init(&pmap);
}

void path_resolve_local(char* buffer, char* path)
{
	sprintf(buffer, "%s/%s", pmap.startpoint, path);
}


void path_resolve_config(char* buffer, char* path)
{
	sprintf(buffer, "%s/%s", pmap.config, path);
}

void path_resolve_cache(char* buffer, char* path)
{
	sprintf(buffer, "%s/%s", pmap.cache, path);
}

void path_resolve_data(char* buffer, char* path)
{
	sprintf(buffer, "%s/%s", pmap.data, path);
}

void path_resolve_runtime(char* buffer, char* path)
{
	sprintf(buffer, "%s/%s", pmap.runtime, path);
}

