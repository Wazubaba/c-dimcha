#ifndef WAZUSTD_STDPATHS
#define WAZUSTD_STDPATHS

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

#ifdef __linux__
	#include <linux/limits.h>
	#include <unistd.h>
#endif

#ifdef _WIN32
	#include <windows.h>
	#ifdef __CYGWIN__
	#endif
	#ifdef _WIN64
	#endif
#endif

#ifdef _SYSTYPE_BSD  /* BSDs */
#endif

#ifdef __APPLE__
#endif
/**
	Contains functions that resolve to standardized paths for a given
	operating system.

	For linux(posix entirely atm) it uses the XDG Basedir Specification.
	On windows it just about throws everything shamelessly into appdata :P
*/

struct Path_Map
{
	char username[_SC_LOGIN_NAME_MAX];
	char startpoint[PATH_MAX];
	char config[PATH_MAX];
	char cache[PATH_MAX];
	char data[PATH_MAX];
	char runtime[PATH_MAX];
};

void paths_init(struct Path_Map* pmap);

#endif /* ifndef WAZUSTD_STDPATHS */

