/*
	This file is part of dimcha.

	Desktop IMage CHAnger - dimcha
	Copyright © 2017 Wazubaba

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ila.h"

#define FILETYPESLEN 3
static const char* FILETYPES[] = {
	"jpg",
	"png",
	"bmp",
};

char* ila_choose_image(struct ImageListArchive* ila)
{
	srand((unsigned int) time(NULL));
	return (ila->paths[(size_t) rand() % ila->size]);
}

/* https://stackoverflow.com/a/5309508 */
const char* get_filename_ext(const char* path)
{
	const char* dot = strrchr(path, '.');
	if (!dot || dot == path) return "";
	return dot + 1;
}

int ila_export(struct ImageListArchive* ila, char* path)
{
	FILE* fd = fopen(path, "wb");
	if (!fd)
		return 1;


	/* Write-out len of total elements */
	fwrite(&ila->size, sizeof(size_t), 1, fd);

	size_t i;
	for (i = 0; i < ila->size; i++)
	{
		size_t strsize = strlen(ila->paths[i]);
		/* Write size of string */
		fwrite(&strsize, sizeof(size_t), 1, fd);
		/* Write data of string */
		fwrite(ila->paths[i], sizeof(char), strlen(ila->paths[i]), fd);
	}

	fclose(fd);
	return 0;
}

int ila_import(struct ImageListArchive* ila, char* path)
{
	FILE* fd = fopen(path, "rb");
	if (!fd)
		return 1;

	fread(&ila->size, sizeof(size_t), 1, fd);

	ila->paths = malloc(PATH_MAX * ila->size);

	size_t i;
	for (i = 0; i < ila->size; i++)
	{
		size_t strsize;
		fread(&strsize, sizeof(size_t), 1, fd);

		char stringData[strsize + 1];
		fread(stringData, sizeof(char), strsize, fd);
		
		stringData[strsize] = '\0';
		ila->paths[i] = strndup(stringData, strsize);
	}
	fclose(fd);

	return 0;
}


int ila_generate_from_path(struct ImageListArchive* ila, char* path)
{

	printf("Accessing '%s'...\n", path);
	DIR* target = opendir(path);

	if (!target)
	{
		perror("Failed to open directory for reading");
		return 1;
	}

	ila->size = 0;
	ila->paths = malloc(sizeof(char*));

	struct dirent *dent;

	while ((dent = readdir(target)) != NULL)
	{
		size_t i;
		for (i = 0; i < FILETYPESLEN; i++)
		{
			// Do not permit anything with a semicolon
			if (strcspn(dent->d_name, ";") != strlen(dent->d_name)) continue;

			if (strcmp(get_filename_ext(dent->d_name), FILETYPES[i]) == 0)
			{
				char finalPath[PATH_MAX] = {0};
				snprintf(finalPath, PATH_MAX, "%s/%s", path, dent->d_name);

				ila->paths = realloc(ila->paths, sizeof(char*) * ++ila->size);
				ila->paths[ila->size - 1] = malloc(sizeof(char) * strlen(finalPath));
				strcpy(ila->paths[ila->size - 1], finalPath);
//				ila->paths[ila->size - 1] = dent->d_name;
			}
		}
	}

	free(dent);
	closedir(target);
	return 0;
}

int ila_generate(struct ImageListArchive* ila, size_t len, char** paths)
{
	ila->size = len;
	ila->paths = malloc(sizeof(char*) * ila->size);

	if(!ila->paths)
		return 0;

	size_t i;
	for (i = 0; i < ila->size; i++)
		ila->paths[i] = paths[i];

	return 1;
}

void ila_destroy(struct ImageListArchive* ila)
{
	if (ila == NULL) return;
	size_t i;
	if (ila->paths != NULL)
	{
		for (i = 0; i < ila->size; i++)
		{
			if (ila->paths[i] != NULL)
				free(ila->paths[i]);
		}

		free(ila->paths);
	}
}

