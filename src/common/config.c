/*
	This file is part of dimcha.

	Desktop IMage CHAnger - dimcha
	Copyright © 2017 Wazubaba

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * TODO: Implement home-made configuration file lib
*/

#include "config.h"

int config_load(struct Options* opts, char* path)
{
	config_t cfg;
	config_init(&cfg);

	const char* ilapath = NULL;
	const char* command = NULL;

	int daemononly;

	if (!config_read_file(&cfg, path))
	{
		fprintf(stderr, "%s:%d - %s\n",
				config_error_file(&cfg),
				config_error_line(&cfg),
				config_error_text(&cfg));
		config_destroy(&cfg);
		return 1;
	}

	if (!config_lookup_int(&cfg, "delay", &opts->delay))
		opts->delay = 10;

	if (config_lookup_string(&cfg, "ilapath", &ilapath))
		strcpy(opts->ilaPath, ilapath);

	if (config_lookup_string(&cfg, "command", &command))
		strcpy(opts->command, command);

	if (!config_lookup_bool(&cfg, "daemononly", &daemononly))
		opts->daemonOnly = daemononly;

	config_destroy(&cfg);
	return 0;
}

int config_save(struct Options* opts, char* path)
{
	if (path == NULL)
		return 2;

	config_t cfg;
	config_init(&cfg);


	config_setting_t *root =  NULL, *setting = NULL;

	root = config_root_setting(&cfg);

	setting = config_setting_add(root, "delay", CONFIG_TYPE_INT);
	config_setting_set_int(setting, opts->delay);

	setting = config_setting_add(root, "ilapath", CONFIG_TYPE_STRING);
	config_setting_set_string(setting, opts->ilaPath);

	setting = config_setting_add(root, "command", CONFIG_TYPE_STRING);
	config_setting_set_string(setting, opts->command);

	setting = config_setting_add(root, "daemononly", CONFIG_TYPE_BOOL);
	config_setting_set_bool(setting, opts->daemonOnly);

	int errcode = 0;
	errcode = config_write_file(&cfg, path);
	config_destroy(&cfg);

	if (errcode == CONFIG_FALSE)
	{
		fprintf(stderr, "Error writing file: '%s'\n", path);
		return 1;
	}

	return 0;
}

void config_options_destroy(struct Options* opts)
{
	if (opts == NULL) return;
	if (opts->command != NULL)
		free(opts->command);
	if (opts->ilaPath != NULL)
		free(opts->ilaPath);
}

