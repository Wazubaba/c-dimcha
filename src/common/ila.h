/*
	This file is part of dimcha.

	Desktop IMage CHAnger - dimcha
	Copyright © 2017 Wazubaba

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DIMCHA_ILA
#define DIMCHA_ILA

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>
#include <time.h>



struct ImageListArchive
{
	/** Contains all the target image paths */
	char** paths;
	/** Contains the number of strings in paths */
	size_t size;
};

int ila_export(struct ImageListArchive* ila, char* path);
int ila_import(struct ImageListArchive* ila, char* path);
/** Will sanitize the path strings to be free of newlines and ';'s */
int ila_generate_from_path(struct ImageListArchive* ila, char* path);
int ila_generate(struct ImageListArchive* ila, size_t len, char** paths);
void ila_destroy(struct ImageListArchive* ila);

char* ila_choose_image(struct ImageListArchive* ila);

#endif /* ifndef DIMCHA_ILA */

