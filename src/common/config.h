/*
	This file is part of dimcha.

	Desktop IMage CHAnger - dimcha
	Copyright © 2017 Wazubaba

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DIMCHA_CONFIG
#define DIMCHA_CONFIG

#include "config.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <libconfig.h>

#include <linux/limits.h>


/**
 * Contains routines to load, update, and output configuration files
*/

struct Options
{
	int delay; /** Delay between image changes in seconds (delay(ops.delay);) */
	char ilaPath[PATH_MAX]; /** Path to the target ila file */
	char command[PATH_MAX]; /** Command to execute to change wallpaper */
	short daemonOnly; /** Whether to skip launching the client on startup */
};

int config_load(struct Options* opts, char* path);
int config_save(struct Options* opts, char* path);
int config_update(struct Options* opts, char* path);

void config_options_destroy(struct Options* opts);

#endif /* ifndef DIMCHA_CONFIG */

