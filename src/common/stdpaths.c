#include "stdpaths.h"

/*
	If user is root or undefined, use system-wide defaults, otherwise
	use XDG defaults.
*/

void paths_init(struct Path_Map* pmap)
{
	//memset((char*) pmap->username, 0, sizeof(pmap->username));
	char* user = getenv("USER");
	char* temp;
	strcpy(pmap->username, user);
	temp = getcwd(pmap->startpoint, PATH_MAX);

	if (temp == NULL || strncmp(pmap->username, "root", 4) == 0)
	{
		strcpy(pmap->config, "/etc");
		strcpy(pmap->cache, "/var/cache");
		strcpy(pmap->data, "/usr/local/share");
		strcpy(pmap->runtime, "/tmp");
	}
	else
	{
		char* xdg_val = NULL;

		xdg_val = getenv("XDG_CONFIG_HOME");
		if (xdg_val == NULL)
			sprintf(pmap->config, "/home/%s/.config", pmap->username);
		else
			strcpy(pmap->config, xdg_val);

		free(xdg_val);

		xdg_val = getenv("XDG_CACHE_HOME");
		if (xdg_val == NULL)
			sprintf(pmap->cache, "/home/%s/.cache", pmap->username);
		else
			strcpy(pmap->cache, xdg_val);

		free(xdg_val);

		xdg_val = getenv("XDG_DATA_HOME");
		if (xdg_val == NULL)
			sprintf(pmap->data, "/home/%s/.local/share", pmap->username);
		else
			strcpy(pmap->data, xdg_val);

		free(xdg_val);

		xdg_val = getenv("XDG_RUNTIME_DIR");
		if (xdg_val == NULL)
			strcpy(pmap->runtime, "/tmp");
		else
			strcpy(pmap->runtime, xdg_val);

		free(xdg_val);
	}
}

