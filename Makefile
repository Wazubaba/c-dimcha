CC= gcc
CFLAGS= -g -O0 -Wall -Wextra -fdiagnostics-color=always \
				-fno-omit-frame-pointer \
				-Isrc

BIN= dimcha-daemon dimcha-client ila-manager

DAEMONSRC= $(shell find src/daemon -type f -name "*.c") \
					 $(shell find src/common -type f -name "*.c")

DAEMONLD= -lconfig -largtable2 -pthread -lImlib2 -lX11


CLIENTSRC= $(shell find src/client -type f -name "*.c") \
					 src/common/config.c

CLIENTLD= -lconfig -largtable2


ILAMGRSRC= $(shell find src/ilamgr -type f -name "*.c") \
					 src/common/ila.c src/common/paths.c \
					 src/common/stdpaths.c

ILAMGRLD= -largtable2

.PHONY: all clean

all: $(BIN)

dimcha-daemon: $(patsubst %.c, %.o, $(DAEMONSRC))
	$(CC) $(CFLAGS) -o$@ $^ $(DAEMONLD)

dimcha-client: $(patsubst %.c, %.o, $(CLIENTSRC))
	$(CC) $(CFLAGS) -o$@ $^ $(CLIENTLD)

ila-manager: $(patsubst %.c, %.o, $(ILAMGRSRC))
	$(CC) $(CFLAGS) -o$@ $^ $(ILAMGRLD)

clean:
	-$(RM) $(patsubst %.c, %.o, $(DAEMONSRC))
	-$(RM) $(patsubst %.c, %.o, $(CLIENTSRC))
	-$(RM) $(patsubst %.c, %.o, $(ILAMGRSRC))
	-$(RM) $(BIN)

