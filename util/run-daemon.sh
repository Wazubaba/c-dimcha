#!/bin/bash
function show_help
{
	echo "Usage: $1 {mode}"
	echo "Valid modes:"
	echo -e "\t-c  --force-config  -  bypass runfile and load config"
	echo -e "\t-i  --force-ila     -  bypass config setting and load ila"
	echo -e "\t-d  --debug         -  start in gdb"
	echo -e "\t-v  --valgrind      -  start in valgrind"
	echo -e "\t-h  --help          -  show this help and exit"
}

configPath="test/dimcharc"
ilaPath="test/herewego.ila"

if [ $# -ge 1 ]; then
	case $1 in 
		-c | --force-config)
			./dimcha-daemon --verbose --runfile-path=test/dimcha.run \
				--force-config --config-path=test/dimcharc \
				--ila-path=test/sample.ila
		;;
		-i | --force-ila)
			./dimcha-daemon --verbose --runfile-path=test/dimcha.run \
				--config-path=test/dimcharc \
				--ila-path=test/sample.ila
		;;
		-d | --debug)
			gdb --args ./dimcha-daemon --verbose --runfile-path=test/dimcha.run \
				--config-path=test/dimcharc
		;;
		-v | --valgrind)
			valgrind --leak-check=full --log-file="valgrind.log" --track-origins=yes \
				./dimcha-daemon --verbose --runfile-path=test/dimcha.run \
				--config-path=test/dimcharc
		;;
		-h | --help)
			show_help "$0"
		;;
		*)
			echo "Invalid argument $1"
			show_help "$0"
		;;
	esac
else
	./dimcha-daemon --verbose --runfile-path=test/dimcha.run \
		--config-path=$configPath
fi

