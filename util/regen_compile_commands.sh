#!/bin/sh

if [ -e Makefile ]; then
	rm compile_commands.json
	make clean
	bear make
else
	echo "Cannot find makefile, are you in the project root?"
fi

