Have a dimcharc.last file that stores the options as a binary struct
on disc that is loaded back into memory instead of the config via
commandline option `--runfile <file>`. If the runfile does not exist,
simply load from the config and emit a warning if verbosity is turned
on (also respect verbosity with syslog stuff).

The only part of dimcha that even needs the config is the daemon
itself. That way we load the config **once** instead of pointlessly
within the client front-end...

TODO: look into transparent backgrounds from within ncurses
(maybe check out htop sources for this? It can do it). Also hit up
syner on his work with ioctl manipulation of the terminals stuffs...

Alternate socket locations should not go into the config, only the
information that controls the main loop should. All initialization
stuff on the other hand belongs in command-line argstuffs (look into
GNU optparse or whatever it was called :P)

Also look into the proper way to launch a text editor on the config
via detecting the $EDITOR envarg perhaps as a commandline arg for
the client?
(Maybe even implement a bare-bones one as a toy? :3) teehee

